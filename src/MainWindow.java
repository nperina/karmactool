import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.xml.sax.SAXException;
import org.eclipse.swt.widgets.Button;
import org.apache.commons.io.*;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.custom.StyledText;

public class MainWindow {

	protected Shell shlFolderMover;
	private Text listOfFilesPath;
	private CLabel lblProgress;
	private Button btnMove;
	private Button btnCopy;
	private Text inputFolderInput;
	private Text outputFolderInput;
	private Text summaryCSVInput;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			MainWindow window = new MainWindow();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlFolderMover.open();
		shlFolderMover.layout();
		while (!shlFolderMover.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	
	private void writeProgressMessage(String message, boolean enable) {
		Display.getDefault().asyncExec(new Runnable() {
			 @Override 
           public void run()
           {
          	 System.out.println("run() in AsyncExec:");
          	 lblProgress.setVisible(true);
          	 lblProgress.setText(message);
          	 btnMove.setEnabled(enable);
          	 btnCopy.setEnabled(enable);
           }
       });
	}
	
	private void moveFiles(String moveFromPath, String moveToPath, String listFilesPath, boolean copy) {
		
		BufferedReader br = null;
		String line = "";
		String csvSplitter = ",";
		ArrayList<String> filenames = new ArrayList<String>();
		
		writeProgressMessage("Reading file list...", false);
		
		try {
			br = new BufferedReader(new FileReader(listFilesPath));
			while((line = br.readLine()) != null) {
				String[] input = line.split(csvSplitter);
				if (input.length > 1) {
					filenames.add(input[1]);
				}
				else if (input.length == 1) {
					filenames.add(input[0]);
				}
			}
			
		} catch(FileNotFoundException e) {
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		FileListFilter filter = new FileListFilter(filenames);
		File moveFromDir = new File(moveFromPath);
		File moveToDir = new File(moveToPath);
		
		if (!moveToDir.isDirectory()) {
			writeProgressMessage("Move to file is not a directory.", false);
			return;
		}
		if(!moveFromDir.isDirectory()) {
			writeProgressMessage("Move from file is not a directory.", false);
			return; 
		}
		File[] listOfFiles = moveFromDir.listFiles(filter);
		File file; 
		for (int i = 0; i < listOfFiles.length; i++) {
			file = listOfFiles[i];
			if (file.isDirectory()) {
				try {
					
					if (copy) {
						writeProgressMessage("Copying directory: " + file.getName(), false);
						moveToDir = new File(moveToPath + "\\" + file.getName());
						FileUtils.copyDirectory(file, moveToDir);
					}
					else {
						writeProgressMessage("Moving directory: " + file.getName(), false);
						FileUtils.moveDirectoryToDirectory(file, moveToDir, true);
					}
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (file.isFile()) {
				try {
					if(copy) {
						FileUtils.copyFile(file, moveToDir);
					}
					else {
						writeProgressMessage("Moving file: " + file.getName(), false);
						FileUtils.moveFile(file, moveToDir);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		writeProgressMessage("Finished", true);
		
	}
	
	
	
	private Thread createMovingThread(String moveFromPath, String moveToPath, String listFilesPath, boolean copy) {
		return new Thread(new Runnable() {
	        @Override
	        public void run() {
					//System.out.println("Called ValidateAlto()");
				moveFiles(moveFromPath, moveToPath, listFilesPath, copy);
	            try {
	            Thread.sleep(1000);
	            } catch (InterruptedException e) {
	            e.printStackTrace();
	            }
		        
	       }
		});

		
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlFolderMover = new Shell();
		shlFolderMover.setSize(615, 487);
		shlFolderMover.setText("Karmac Tool");
		
		DirectoryDialog dirSelector = new DirectoryDialog(shlFolderMover, SWT.OPEN);
		FileDialog fileSelector = new FileDialog(shlFolderMover, SWT.OPEN);
		
		FileDialog schemaFileSelector = new FileDialog(shlFolderMover, SWT.OPEN);
		DirectoryDialog altoDirSelector = new DirectoryDialog(shlFolderMover, SWT.OPEN);
		
		TabFolder tabFolder = new TabFolder(shlFolderMover, SWT.NONE);
		tabFolder.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		tabFolder.setBounds(0, 0, 599, 448);
		
		/* FOLDER MOVER TAB */
		TabItem tbtmFolderMover = new TabItem(tabFolder, SWT.NONE);
		tbtmFolderMover.setText("Folder Mover");
		
		Composite cmpFolderMover = new Composite(tabFolder, SWT.NONE);
		tbtmFolderMover.setControl(cmpFolderMover);

		CLabel lblSchemaFilePath = new CLabel(cmpFolderMover, SWT.NONE);
		lblSchemaFilePath.setBounds(10, 27, 129, 21);
		lblSchemaFilePath.setText("Move from folder:");
		
		Text moveFolderFilepath = new Text(cmpFolderMover, SWT.BORDER);
		moveFolderFilepath.setBounds(10, 64, 329, 21);
		
		CLabel lblToFolder = new CLabel(cmpFolderMover, SWT.NONE);
		lblToFolder.setBounds(10, 91, 129, 21);
		lblToFolder.setText("To folder:");
		
		Button btnOpenMoveFromDialog = new Button(cmpFolderMover, SWT.NONE);
		btnOpenMoveFromDialog.setBounds(351, 62, 25, 25);
		btnOpenMoveFromDialog.setText("...");
		
		Text toFolderFilepath = new Text(cmpFolderMover, SWT.BORDER);
		toFolderFilepath.setBounds(10, 118, 329, 21);
		
		Button btnOpenMoveToDialog = new Button(cmpFolderMover, SWT.NONE);
		btnOpenMoveToDialog.setBounds(351, 116, 25, 25);
		btnOpenMoveToDialog.setText("...");
		
		CLabel lblNewLabel = new CLabel(cmpFolderMover, SWT.NONE);
		lblNewLabel.setBounds(10, 145, 202, 21);
		lblNewLabel.setText("List of files (.csv format):");
		
		listOfFilesPath = new Text(cmpFolderMover, SWT.BORDER);
		listOfFilesPath.setBounds(10, 172, 329, 21);
		
		Button btnOpenListOfFilesDialog = new Button(cmpFolderMover, SWT.NONE);
		btnOpenListOfFilesDialog.setBounds(351, 170, 25, 25);
		btnOpenListOfFilesDialog.setText("...");
		
		lblProgress = new CLabel(cmpFolderMover, SWT.NONE);
		lblProgress.setBounds(208, 216, 264, 21);
		lblProgress.setText("");
		
		btnMove = new Button(cmpFolderMover, SWT.NONE);
		btnMove.setBounds(10, 212, 75, 25);
		btnMove.setText("Move!");
		
		btnCopy = new Button(cmpFolderMover, SWT.NONE);
		btnCopy.setText("Copy!");
		btnCopy.setBounds(106, 212, 75, 25);
		
		btnOpenListOfFilesDialog.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String result = fileSelector.open();
				if (result == null) {
					listOfFilesPath.setText("");
				}
				else {
					listOfFilesPath.setText(result);
				}
			}
		});
		
		btnOpenMoveToDialog.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String result = dirSelector.open();
				if (result == null) {
					toFolderFilepath.setText("");
				}
				else {
					toFolderFilepath.setText(result);
				}
			}
		});
		
		
		
		btnMove.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(org.eclipse.swt.widgets.Event event) {
				String moveFromPath = moveFolderFilepath.getText();
				String moveToPath = toFolderFilepath.getText();
				String listPath = listOfFilesPath.getText();
				
				if (moveFromPath.equals("") || moveToPath.equals("") || listPath.equals("") || (!(listPath.endsWith("csv")))){
					lblProgress.setText("Input Error!");
					return; 
				}
				else {
					Thread moverThread = createMovingThread(moveFromPath, moveToPath, listPath, false);
					moverThread.start();
				}
			}
		});
		
		btnCopy.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(org.eclipse.swt.widgets.Event event) {
				String moveFromPath = moveFolderFilepath.getText();
				String moveToPath = toFolderFilepath.getText();
				String listPath = listOfFilesPath.getText();
				
				if (moveFromPath.equals("") || moveToPath.equals("") || listPath.equals("") || (!(listPath.endsWith("csv")))){
					lblProgress.setText("Input Error!");
					return; 
				}
				else {
					Thread moverThread = createMovingThread(moveFromPath, moveToPath, listPath, true);
					moverThread.start();
				}
			}
		});
		
		btnOpenMoveFromDialog.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String result = dirSelector.open();
				if (result == null) {
					moveFolderFilepath.setText("");
				}
				else {
					moveFolderFilepath.setText(result);
				}
			}
		});
		
		
		/* INPUT ANALYZER TAB */ 
		TabItem tbtmInputAnalyzer = new TabItem(tabFolder, SWT.NONE);
		tbtmInputAnalyzer.setText("Input Analyzer");
		
		Composite cmpInputAnalyzer = new Composite(tabFolder, SWT.NONE);
		tbtmInputAnalyzer.setControl(cmpInputAnalyzer);
		
		CLabel lblInputFolder = new CLabel(cmpInputAnalyzer, SWT.NONE);
		lblInputFolder.setBounds(32, 38, 168, 21);
		lblInputFolder.setText("Input Folder:");
		
		inputFolderInput = new Text(cmpInputAnalyzer, SWT.BORDER);
		inputFolderInput.setBounds(32, 65, 392, 21);
		
		Button btnAnalyzeInput = new Button(cmpInputAnalyzer, SWT.NONE);
		
		btnAnalyzeInput.setBounds(33, 155, 75, 25);
		btnAnalyzeInput.setText("Analyze!");
		
		Button btnOpenInputFolderDialog = new Button(cmpInputAnalyzer, SWT.NONE);
		btnOpenInputFolderDialog.setText("...");
		btnOpenInputFolderDialog.setBounds(439, 63, 25, 25);
		
		StyledText inputProgress = new StyledText(cmpInputAnalyzer, SWT.WRAP);
		inputProgress.setEditable(false);
		inputProgress.setEnabled(false);
		inputProgress.setBounds(195, 155, 309, 242);
		
		// EVENTS
		btnOpenInputFolderDialog.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				String result = dirSelector.open();
				if (result == null) {
					inputFolderInput.setText("");
				}
				else {
					inputFolderInput.setText(result);
				}
			}
		});
		
		btnAnalyzeInput.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (inputFolderInput.getText().equals("")) {
					inputProgress.setText("Input is Empty!");
				}
				else {
					InputAnalyzer inputAnalyzer = new InputAnalyzer(inputFolderInput.getText(), inputProgress, btnAnalyzeInput);
					Thread analysis = inputAnalyzer.getThread();
					analysis.start();
				}
				
				
			}
		});
		
		
		
		
		/* OUTPUT ANALYZER TAB */
		TabItem tbtmOutputAnalyzer = new TabItem(tabFolder, SWT.NONE);
		tbtmOutputAnalyzer.setText("Output Analyzer");
		
		Composite outputAnalyzerTab = new Composite(tabFolder, SWT.NONE);
		tbtmOutputAnalyzer.setControl(outputAnalyzerTab);
		
		CLabel lblOutputFolder = new CLabel(outputAnalyzerTab, SWT.NONE);
		lblOutputFolder.setBounds(26, 24, 149, 21);
		lblOutputFolder.setText("Output Folder:");
		
		outputFolderInput = new Text(outputAnalyzerTab, SWT.BORDER);
		outputFolderInput.setBounds(26, 54, 392, 21);
		
		Button btnOpenOutputFolderDialog = new Button(outputAnalyzerTab, SWT.NONE);
		btnOpenOutputFolderDialog.setText("...");
		btnOpenOutputFolderDialog.setBounds(424, 54, 25, 21);
		
		summaryCSVInput = new Text(outputAnalyzerTab, SWT.BORDER);
		summaryCSVInput.setBounds(26, 108, 392, 21);
		
		CLabel lblInputSummary = new CLabel(outputAnalyzerTab, SWT.NONE);
		lblInputSummary.setText("Input Summary CSV:");
		lblInputSummary.setBounds(26, 81, 149, 21);
		
		Button btnOpenSummaryCSVDialog = new Button(outputAnalyzerTab, SWT.NONE);
		btnOpenSummaryCSVDialog.setText("...");
		btnOpenSummaryCSVDialog.setBounds(424, 108, 25, 21);
		
		Button btnAnalyzeOutput = new Button(outputAnalyzerTab, SWT.NONE);
		btnAnalyzeOutput.setText("Analyze!");
		btnAnalyzeOutput.setBounds(26, 164, 75, 25);
		
		StyledText outputProgress = new StyledText(outputAnalyzerTab, SWT.WRAP);
		outputProgress.setEnabled(false);
		outputProgress.setEditable(false);
		outputProgress.setBounds(153, 164, 309, 242);
		
		// EVENTS
		btnOpenSummaryCSVDialog.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String result = fileSelector.open();
				if (result == null) {
					summaryCSVInput.setText("");
				}
				else {
					summaryCSVInput.setText(result);
				}
			}
		});
		
		btnOpenOutputFolderDialog.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String result = dirSelector.open();
				if (result == null) {
					outputFolderInput.setText("");
				}
				else {
					outputFolderInput.setText(result);
				}
			}
		});
		
		
		
		
		
		btnAnalyzeOutput.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (outputFolderInput.getText().equals("")) {
					inputProgress.setText("Output Folder path is Empty!");
				}
				else if (summaryCSVInput.getText().equals("")) {
					inputProgress.setText("Summary CSV is Empty!");
				}
				else {
					OutputAnalyzer outputAnalyzer = new OutputAnalyzer(outputFolderInput.getText(), summaryCSVInput.getText(), outputProgress, btnAnalyzeOutput);
					Thread analysis = outputAnalyzer.getThread();
					analysis.start();
				}
				
				
			}
		});
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//EVENTS
		
		
		
		
		
		

	}
}
