import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

public class ExtensionFilter implements FileFilter {
	private String extension; 
	public ExtensionFilter(String extension) {
		this.extension = extension; 
	}
	
	
	@Override
	public boolean accept(File file) {
		// TODO Auto-generated method stub
		if (file.getName().endsWith(extension)) {
			return true; 
		}
		else {
			return false; 
		}
	}
}
