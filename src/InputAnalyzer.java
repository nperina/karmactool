import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;

public class InputAnalyzer {
	
	private String inputPath; 
	private String outputPath; 
	private String concordancePath; 
	private StyledText progressLabel;
	private Button analyzeBtn; 
	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";
	private static final String CSV_HEADER = "ID, file_count,master_count,"
			+ "master_names,access_count,access_names,mets_count,mets_names,alto_count,alto_names,text_count,text_names,pdf_count,pdf_names,"
			+ "tiff_status,tiff_errors";
	private String[] headers = { "master", "access", "mets", "alto", "text", "pdf"};
	
	public InputAnalyzer(String input, StyledText progressLabel, Button btn) {
		inputPath = input; 
		concordancePath = input + "\\concordance table\\"; 
		this.progressLabel = progressLabel;
		this.analyzeBtn = btn; 
		this.outputPath = concordancePath; 
	}
	
	
	
	private LinkedHashMap<String, ArrayList<ArrayList<String>>> analyzeConcordance() {
		System.out.println("concordance Path: " + concordancePath);
		File concordanceDir = new File(concordancePath);
		
		BufferedReader br; 
		
		ExtensionFilter filter = new ExtensionFilter(".csv");
		File[] concordanceCSVs = concordanceDir.listFiles(filter);
		
		LinkedHashMap<String, ArrayList<ArrayList<String>>> inventory = new LinkedHashMap<String, ArrayList<ArrayList<String>>>();

		for (int i = 0; i < concordanceCSVs.length; i++) {
			System.out.println("File:");
			System.out.println(concordanceCSVs[i].getName());
			String line; 
			File concordanceFile = concordanceCSVs[i];
			String id; 
			String volgnr; 
			String master; 
			String access; 
			String mets;
			String alto; 
			String text; 
			String pdf; 
			try {
				br = new BufferedReader(new FileReader(concordanceFile.getPath()));
				//skip header line
				br.readLine(); 
				while((line = br.readLine()) != null) {
					String[] input = line.split(";");
					
					
					
					if (input.length > 1) {
						id = input[0].replaceAll("^\"|\"$", "");
						volgnr = input[1].replaceAll("^\"|\"$", "");
						master = input[2].replaceAll("^\"|\"$", "");
						access = input[3].replaceAll("^\"|\"$", "");
						mets = input[4].replaceAll("^\"|\"$", "");
						alto = input[5].replaceAll("^\"|\"$", "");
						text = input[6].replaceAll("^\"|\"$", "");
						pdf = input[7].replaceAll("^\"|\"$", "");
						ArrayList<ArrayList<String>> files;
						ArrayList<String> fileTypeList;
						if (inventory.containsKey(id)) {
							 files = inventory.get(id);
							 for (int j = 0; j < headers.length; j++) {
								 fileTypeList = files.get(j);
								 int inputIndex = j + 2; 
								 if (headers[j].equals("master") || headers[j].equals("access")
										 || headers[j].equals("alto") || headers[j].equals("text") ) {
									 fileTypeList.add(input[inputIndex].replaceAll("\"", ""));
								 }
							 }
						}
						else {
							files = new ArrayList<ArrayList<String>>();
							for (int j = 0; j < headers.length; j++) {
								fileTypeList  = new ArrayList<String>(); 
								int inputIndex = j + 2; 
								fileTypeList.add(input[inputIndex].replaceAll("\"", ""));
								files.add(fileTypeList);
								
							}
							inventory.put(id, files);
						}
					}
					
				}
				System.out.println("Finished inventory for file: " + concordanceFile.getName());
				
			} catch(FileNotFoundException e) {
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return inventory; 
		
		
	}
	
	private void writeProgressMessage(String message, boolean enable) {
		Display.getDefault().asyncExec(new Runnable() {
			 @Override 
           public void run()
           {
			 progressLabel.setText(message);
			 analyzeBtn.setEnabled(enable);
           }
       });
	}
	private void finish() {
		writeProgressMessage("Finished. Output is here: " + outputPath, true);
	}
	
	private String checkTiff(String id, ArrayList<String> masterFilenames) {
		String errMessage = "";
		
		String tiffDirectoryPath = inputPath + "//" + id + "//tiff";
		File tiffDirectory = new File(tiffDirectoryPath);
		if (!(tiffDirectory.exists())) {
			errMessage = "Directory not found: " + tiffDirectory.getPath();
			return errMessage;
		}
		else {
			ExtensionFilter tiffFilter = new ExtensionFilter(".tif");
			File[] listOfTiffs = tiffDirectory.listFiles(tiffFilter);
			if (listOfTiffs.length == 0) {
				errMessage = errMessage + "0 tiff files found.";
			}
			ArrayList<String> tiffNames = new ArrayList<String>();
			for (int f = 0; f < listOfTiffs.length; f++) {
				String tiffName = listOfTiffs[f].getName();
				tiffNames.add(tiffName);
				
			}
			for (String name : masterFilenames) {
				System.out.println("name: ");
				System.out.println(name);
			}
			if (listOfTiffs.length >= masterFilenames.size()) {
				for (int f = 0; f < listOfTiffs.length; f++) {
					String tiffName = listOfTiffs[f].getName();
					String masterName = tiffName.replaceAll("tiff.tif", "master.jp2");
					System.out.println("master name: ");
					System.out.println(masterName);
					if (masterFilenames.indexOf(masterName) == -1) {
						errMessage = errMessage + " tiff file " + listOfTiffs[f].getName() + " found in directory but equivalent master filename not found in concordance table. ";
					}
				}
			}
			else {
				for (String masterName : masterFilenames) {
					String tiffName = masterName.replace("master.jp2", "tiff.tif");
					if (tiffNames.indexOf(tiffName) == -1) {
						errMessage = errMessage + " master file " + masterName + " found in concordance table but equivalent tiff file not found in tiff direcoty. ";
					}
				}
				
				
			}
		}
		return errMessage; 
		
	}
	
	private void writeSummary(LinkedHashMap<String, ArrayList<ArrayList<String>>> inventory) {
		int num = 0; 
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH_mm_ss");  
		LocalDateTime now = LocalDateTime.now();
		outputPath = inputPath + "\\" + dtf.format(now) + "_summary.csv";
		
	
		try {
			FileWriter fileWriter = new FileWriter(outputPath);
			fileWriter.append(CSV_HEADER);
			fileWriter.append(NEW_LINE_SEPARATOR);
			inventory.forEach((id, files) -> {
				
				System.out.println(); 
	            System.out.println("ID: ");
	            System.out.println(id);
	            try {
					fileWriter.append(id);
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(Integer.toString(files.get(0).size()));
					fileWriter.append(COMMA_DELIMITER);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            String fileTypeName; 
	            ArrayList<String> fileNames; 
	            String errMessage = ""; 
	            for (int i = 0; i < headers.length; i++) {
	            	fileTypeName = headers[i];
	            	fileNames = files.get(i);
	            	if (i == 0) {
	            		errMessage = checkTiff(id, fileNames);
	            	}
	            	if (errMessage.equals("")) {
	            		System.out.println("no tiff errors");
	            	}
	            	else {
	            		System.out.println("tiff errors: " + errMessage);
	            	}
	            	System.out.println("Type: " + headers[i]);
	            	System.out.println("Size: " + Integer.toString(fileNames.size()));
	            	String fileNamesString = ""; 
	            	int index = 0; 
	            	fileNamesString = String.join(";", fileNames);
	            	try {
						fileWriter.append(Integer.toString(fileNames.size()));
						fileWriter.append(COMMA_DELIMITER);
						fileWriter.append(fileNamesString);
						fileWriter.append(COMMA_DELIMITER);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	            try {
	            	if(errMessage.equals("")) {
	            		fileWriter.append("");
	            	}
	            	else {
	            		fileWriter.append("error");
	            	}
	            	fileWriter.append(COMMA_DELIMITER);
	            	fileWriter.append(errMessage);
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(NEW_LINE_SEPARATOR);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            
				
				
				
	        });
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}

		
		

		
	}
	
	
	
	private void begin() {
		writeProgressMessage("Analyzing...", false);
		LinkedHashMap<String, ArrayList<ArrayList<String>>> inventory = analyzeConcordance(); 
		writeSummary(inventory);
		finish();
	}
	
	
	
	
	public Thread getThread() {
		return new Thread(new Runnable() {
	        @Override
	        public void run() {
					//System.out.println("Called ValidateAlto()");
	        	begin(); 
	            try {
	            Thread.sleep(1000);
	            } catch (InterruptedException e) {
	            e.printStackTrace();
	            }
		        
	       }
		});
	}
	
	

}
