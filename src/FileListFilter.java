import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

public class FileListFilter implements FileFilter {
	private ArrayList<String> names; 
	
	public FileListFilter(ArrayList<String> acceptedNames) {
		this.names = acceptedNames;
	}
	
	
	@Override
	public boolean accept(File file) {
		// TODO Auto-generated method stub
		if (this.names.contains(file.getName())) {
			return true; 
		}
		else {
			return false; 
		}
	}
}
