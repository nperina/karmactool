import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;

public class OutputAnalyzer {
	
	private String outputPath; 
	private String inputSummaryPath;
	private StyledText progressLabel; 
	private Button analyzeBtn;
	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";
	private final String[] inputHeaders = {"ID", "file_count","master_count","master_names",
			"access_count","access_names", "mets_count", "mets_names" , "alto_count", "alto_names","text_count", "text_names", "pdf_count",
			"pdf_names","tiff_status", "tiff_errors"
	};
	/*private final String[] outputSummaryHeaders = {"ID", "has_errors?", "master_errors", "master_count", "master_files_found", "access_errors", "access_count", "access_files_found",
			"mets_errors", "mets_count", "mets_files_found", "alto_errors", "alto_count", "alto_files_found", "text_errors", "text_count", "text_files_found",
			"pdf_errors", "pdf_count", "pdf_files_found"
	};*/
	private final String[] outputSummaryHeaders = {"ID", "has_errors?", "master_errors", "master_count",  "access_errors", "access_count",
			"mets_errors", "mets_count", "alto_errors", "alto_count", "text_errors", "text_count", 
			"pdf_errors", "pdf_count"
	};
	private final String[] fileTypes = { "master", "access", "mets", "alto", "text", "pdf"};
	private final String[] fileExtensions = {".jp2", ".jp2", ".xml", ".xml", ".xml", ".pdf"};

	public OutputAnalyzer(String outputPath, String inputSummaryPath, StyledText progressLabel, Button btn) {
		this.outputPath = outputPath; 
		this.inputSummaryPath = inputSummaryPath;
		this.progressLabel = progressLabel; 
		this.analyzeBtn = btn; 
	}
	
	public Thread getThread() {
		return new Thread(new Runnable() {
	        @Override
	        public void run() {
					//System.out.println("Called ValidateAlto()");
	        	analyzeOutput(); 
	            try {
	            Thread.sleep(1000);
	            } catch (InterruptedException e) {
	            e.printStackTrace();
	            }
		        
	       }
		});
	}
	
	
	private String[] checkDirectory(String dirName, String ext, String dirPath, ArrayList<String> fileNames, int fileCount) {
		System.out.println("Checking directory: " + dirName);
		System.out.println("directory path: " + dirPath);
		String[] result = new String[3];
		String errMessage = "";
		File searchDir = new File(dirPath);
		if (!searchDir.isDirectory()) {
			errMessage = "Cannot find directory: " + dirPath;
			result[0] = "";
			result[1] = "0";
			result[2] = errMessage;
			return result; 
		}
		else {
			ExtensionFilter extensionFilter = new ExtensionFilter(ext);
			File[] actualFilesWithExt = searchDir.listFiles(extensionFilter);
			ArrayList<String> actualFileNames = new ArrayList<String>();
			String filesFound = "";
			for (int i =0; i < actualFilesWithExt.length; i++) {
				String actualName = actualFilesWithExt[i].getName();
				filesFound += actualName + ";";
				if (!fileNames.contains(actualName) && !dirName.equals("text")) {
					errMessage += " File " + actualName + " is in " + dirName + " directory but not in summary file. ";
				}
			}
			System.out.println("fileFound:" + filesFound);
			System.out.println("");
			filesFound = filesFound.substring(0, filesFound.length() - 1);
			int actualFileCount = actualFilesWithExt.length; 
			if(actualFileCount < fileCount && !dirName.equals("text")) {
				for (String name : fileNames) {
					if (!actualFileNames.contains(name)) {
						errMessage += " File " + name + " found in summary file but not in " + dirName + " directory. ";
					}
				}
			}
			result[0] = filesFound; 
			result[1] = Integer.toString(actualFileCount);
			result[2] = errMessage; 
			return result;
		}
	}
	
	private void writeProgressMessage(String message, boolean enable) {
		Display.getDefault().asyncExec(new Runnable() {
			 @Override 
           public void run()
           {
			 progressLabel.setText(message);
			 analyzeBtn.setEnabled(enable);
           }
       });
	}
	
	
	private void analyzeOutput() {
		writeProgressMessage("Analyzing....", false);
		File outputDir = new File(outputPath);
		if ( !outputDir.isDirectory()) {
			writeProgressMessage("Out put directory not found.", true);
			return; 
		}
		File summaryCSV = new File(inputSummaryPath);
		if (!summaryCSV.exists()) {
			writeProgressMessage("Summary csv not found.", true);
			return;
		}
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH_mm_ss");  
		LocalDateTime now = LocalDateTime.now();
		String outputSummaryPath = outputPath + "\\" + dtf.format(now) + "_output_summary.csv";
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(summaryCSV.getPath()));
			FileWriter fileWriter = new FileWriter(outputSummaryPath);
			fileWriter.append(String.join(",", outputSummaryHeaders));
			fileWriter.append(NEW_LINE_SEPARATOR);
			//skip header line
			br.readLine(); 
			String line;
			String id; 
			String fileCount;
			String mets; 
			
			while((line = br.readLine()) != null) {
				String[] input = line.split(",");
				id = input[0].replaceAll("\"", "");
				System.out.println("line: ");
				System.out.println(line);
				String lineResults = "";
				
				int errorCount = 0; 
				String hasErrors = "";
				File idDir = new File(outputPath + "\\" + id  + "\\");
				boolean outerDirExists = idDir.isDirectory();
				for (int i = 0; i < fileTypes.length; i++) {
					String fileType = fileTypes[i];
					
					String searchDirPath = outputPath + "\\" + id + "\\" + fileType + "\\";
					int countIndex = 2 + (i * 2);
					int namesIndex = countIndex + 1; 
					int fileTypeCount = Integer.parseInt(input[countIndex].replaceAll("\"", ""));
					String fileNamesString = input[namesIndex].replaceAll("\"", "");
					ArrayList<String> fileNames = new ArrayList(Arrays.asList(fileNamesString.split(";")));
					String fileExt = fileExtensions[i];
					String filesFound = ""; 
					String actualFileCount = ""; 
					String errMessage = ""; 
					if (outerDirExists) {
						String result[] = checkDirectory(fileType, fileExt, searchDirPath, fileNames, fileTypeCount);
						filesFound = result[0];
						actualFileCount = result[1];
						errMessage = result[2];
					}
					else {
						hasErrors = "error  - ID directory does not exist. ";
						errMessage = "";
						actualFileCount = "0";
						filesFound = "";
					}
					//lineResults = lineResults + COMMA_DELIMITER + errMessage + COMMA_DELIMITER + actualFileCount + COMMA_DELIMITER + filesFound;
					lineResults = lineResults + COMMA_DELIMITER + errMessage + COMMA_DELIMITER + actualFileCount;
					if (!errMessage.equals("")) {
						hasErrors = "error";
					}
				}
				lineResults = id + COMMA_DELIMITER + hasErrors + lineResults + NEW_LINE_SEPARATOR;
				fileWriter.append(lineResults);
			
				
			}
			fileWriter.flush();
			fileWriter.close();
			writeProgressMessage("Finished. Output is here: " + outputSummaryPath, true);
			
		} catch(FileNotFoundException e) {
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	
	
	

}
}
